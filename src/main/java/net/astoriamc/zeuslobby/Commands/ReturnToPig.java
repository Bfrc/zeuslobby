package net.astoriamc.zeuslobby.Commands;

import net.astoriamc.zeuslobby.ZeusLobby;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class ReturnToPig implements CommandExecutor {
    private ZeusLobby plugin = ZeusLobby.getPlugin(ZeusLobby.class);

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return true;
        Player player = (Player) sender;
        Entity entity = this.plugin.getPig().get(player);
        entity.addPassenger(player);
        return true;
    }
}
