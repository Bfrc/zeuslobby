package net.astoriamc.zeuslobby.listeners;

import net.astoriamc.zeuslobby.ZeusLobby;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;

public class onLeave implements Listener {
    ZeusLobby zeusLobby = ZeusLobby.getPlugin(ZeusLobby.class);
    @EventHandler
    public void onPlayerLeaveEntity(PlayerQuitEvent event) {
        HashMap<Player, Entity> pig = zeusLobby.getPig();
        pig.get(event.getPlayer()).remove();
        pig.remove(event.getPlayer());
        zeusLobby.setPig(pig);
    }
}
