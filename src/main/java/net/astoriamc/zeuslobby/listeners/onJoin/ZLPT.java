package net.astoriamc.zeuslobby.listeners.onJoin;

import net.astoriamc.zeuslobby.ZeusLobby;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;


import java.util.HashMap;

public class ZLPT {
    public ZLPT(ZeusLobby plugin, Player player, Entity boat, int speed, int goX, int goY) {
        Runnable r = new ss(player);
        new Thread(r).start();
        boolean alreadyExecuted = false;
        while (true) {
            Vector pos = boat.getLocation().toVector();
            if (!player.isOnline()) {
                return;
            }
            if (boat.getLocation().getX() < goX) {
                alreadyExecuted = false;
                    Location targetLocation = new Location(boat.getWorld(), goX+10, goY, 0.0D, 0.0F, 0.0F);
                    Vector target = targetLocation.toVector();
                    Vector velocity = target.subtract(pos);
                    try {
                        boat.setVelocity(velocity.normalize().multiply(speed)); //ERROR
                    }catch (IllegalArgumentException exception) {
                        World world = boat.getWorld();
                        Location l = new Location(world, 1, goY, 0);
                        new call(l, player, boat).runTask(plugin);
                    }
            } else {
                if (!alreadyExecuted) {
                    World world = boat.getWorld();
                    Location l = new Location(world, 1, goY, 0);
                    new call(l, player, boat).runTask(plugin);
                    alreadyExecuted = true;
                }
            }
        }
    }

    private String FS(String a, String b) {
        return ChatColor.translateAlternateColorCodes('&', "&" + a + "W&" + b + "e&" + a + "l&" + b + "c&" + a + "o&" + b + "m&" + a + "e &" + b + "t&" + a + "o &" + b + "A&" + a + "s&" + b + "t&" + a + "o&" + b + "r&" + a + "i&" + b + "a&" + a + "M&" + b + "C");
    }

    private class ss implements Runnable {
        private final Player p;

        private ss(Player player) {
            p = player;
        }

        @Override
        public void run() {
            p.sendTitle(FS("e", "6"), "", 2, 2, 0);
            p.sendTitle(FS("6", "e"), "", 0, 2, 0);
            p.sendTitle(FS("e", "6"), "", 0, 2, 0);
            p.sendTitle(FS("6", "e"), "", 0, 2, 2);
        }
    }

    public class call extends BukkitRunnable {
        private final Location location;
        private final Player player;
        private final Entity entity;

        public call(Location location, Player player, Entity entity) {
            this.entity = entity;
            this.player = player;
            this.location = location;
        }

        @Override
        public void run() {
            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10,  10));
            entity.eject();
            entity.teleport(location);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            entity.addPassenger(player);
        }
    }
}
