package net.astoriamc.zeuslobby.listeners.onJoin;

import net.astoriamc.zeuslobby.Utils.Items;
import net.astoriamc.zeuslobby.ZeusLobby;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;

public class ZeusEventOnJoin implements Listener {
    private final ZeusLobby plugin;
    private Player player;
    private Entity pig;

    public ZeusEventOnJoin(ZeusLobby plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        System.out.println(1);
        this.player = player;
        World world = player.getWorld();
        player.getInventory().clear();
        Thread thread = new Thread(new ss(), player.getDisplayName());
        pig = world.spawnEntity(new Location(world, 0d, 120, 0d, 0f, 0f), EntityType.PIG);
        System.out.println(1);
        HashMap<Player, Entity> pigMap = this.plugin.getPig();
        pigMap.put(player, this.pig);
        plugin.setPig(pigMap);
        pig.setGravity(false);
        if (this.plugin.getConfig().getBoolean("InvisiblePig")) {
            ((LivingEntity) pig).addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 2147483647, 1));
        }
        pig.setInvulnerable(true);
        pig.addPassenger(player);
        thread.setDaemon(true);
        thread.start();
    }

    private class ss implements Runnable {
        @Override
        public void run() {
            new ZLPT(plugin, player, pig, plugin.getConfig().getInt("PigSpeed"), plugin.getConfig().getInt("GoToX"), plugin.getConfig().getInt("NormalY"));
        }
    }
}
