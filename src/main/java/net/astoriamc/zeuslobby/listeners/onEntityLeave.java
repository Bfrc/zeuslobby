package net.astoriamc.zeuslobby.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleExitEvent;

public class onEntityLeave implements Listener {
    @EventHandler
    public void onPlayerLeaveEntity(VehicleExitEvent event) {
        Player player = Bukkit.getPlayer(event.getExited().getUniqueId());
        if (player == null) {
            return;
        }
        if (!player.hasPermission("ZeusLobby.leave")) {
            event.getVehicle().addPassenger(player);
        }
    }
}
