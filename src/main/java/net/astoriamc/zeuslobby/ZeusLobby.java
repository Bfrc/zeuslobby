package net.astoriamc.zeuslobby;

import net.astoriamc.zeuslobby.Commands.ReturnToPig;
import net.astoriamc.zeuslobby.listeners.onEntityLeave;
import net.astoriamc.zeuslobby.listeners.onJoin.ZeusEventOnJoin;
import net.astoriamc.zeuslobby.listeners.onLeave;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public final class ZeusLobby extends JavaPlugin {
    private HashMap<Player, Entity> pig = new HashMap<>();

    @Override
    public void onEnable() {
        saveDefaultConfig();
        loadConfig();
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        System.out.println(getConfig().getInt("PigSpeed") + " " + getConfig().getInt("GoToX") + " " + getConfig().getInt("NormalY"));
        new ZeusEventOnJoin(this);
        getCommand("returntopig").setExecutor(new ReturnToPig());
        Bukkit.getPluginManager().registerEvents(new onEntityLeave(), this);
        Bukkit.getPluginManager().registerEvents(new onLeave(), this);
    }

    @Override
    public void onDisable() {
        saveConfig();
    }

    public void loadConfig() {
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public HashMap<Player, Entity> getPig() {
        return this.pig;
    }

    public void setPig(HashMap<Player, Entity> pig) {
        this.pig = pig;
    }
}
