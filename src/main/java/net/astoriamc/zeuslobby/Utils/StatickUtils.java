package net.astoriamc.zeuslobby.Utils;

import net.astoriamc.zeuslobby.ZeusLobby;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.output.ByteArrayOutputStream;
import org.bukkit.entity.Player;

import java.io.DataOutputStream;


public class StatickUtils {
    public static void sendPlayerToServer(Player player, String server, ZeusLobby zeusLobby) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(server);
        } catch (Exception e) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&6AstoriaMC&8] &fThere was an problem connecting to " + server + "!"));
            return;
        }
        player.sendPluginMessage(zeusLobby, "BungeeCord", b.toByteArray());
    }

    public static void CheckIfBungee(ZeusLobby zeusLobby) {
        if (!zeusLobby.getServer().getVersion().contains("Spigot") && !zeusLobby.getServer().getVersion().contains("Paper") && !zeusLobby.getServer().getVersion().contains("Tuinity")) {
            Bukkit.getPluginManager().disablePlugin(zeusLobby);
        } else if (!zeusLobby.getServer().getVersion().contains("Tuinity")) {
            System.out.println("Please use Tuinity for better performance in chunk loading.");
        }
    }
}
