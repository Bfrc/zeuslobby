package net.astoriamc.zeuslobby.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class Items {
    public static ItemStack cloack() {
        ItemStack cloack = new ItemStack(Material.CLOCK);
        ItemMeta itemMeta = cloack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&eLobby Selector"));
        cloack.setItemMeta(itemMeta);
        cloack.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        return cloack;
    }

    public static ItemStack creative() {
        ItemStack cloack = new ItemStack(Material.GRASS_BLOCK);
        ItemMeta itemMeta = cloack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&eLobby Selector"));
        cloack.setItemMeta(itemMeta);
        cloack.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        return cloack;
    }

    public static ItemStack surivival() {
        ItemStack cloack = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta itemMeta = cloack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&eLobby Selector"));
        cloack.setItemMeta(itemMeta);
        cloack.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        return cloack;
    }

    public static ItemStack player(Player player) {
        ItemStack playerHead = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta meta = (SkullMeta) playerHead.getItemMeta();
        meta.setDisplayName("You");
        meta.setOwningPlayer(player);
        playerHead.setItemMeta(meta);
        return playerHead;
    }
}
